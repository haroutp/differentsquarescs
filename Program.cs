﻿using System;
using System.Collections.Generic;

namespace differentSquares
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        int differentSquares(int[][] matrix) {
            HashSet<int> set = new HashSet<int>();

            for(int i = 0; i < matrix.Length - 1; i++){
                for(int j = 0; j < matrix[0].Length - 1; j++){
                    set.Add((matrix[i][j] * 1000) +  (matrix[i][j+1] * 100) + (matrix[i + 1][j] * 10) + matrix[i + 1][j + 1]);
                }
            }
            return set.Count;
        }

    }
}
